# kickstart template for Fedora 8 and later.
# (includes %end blocks)
# do not use with earlier distros

#platform=x86, AMD64, or Intel EM64T
# System authorization information
auth  --useshadow  --enablemd5
# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all --initlabel
# Use text mode install
text
# Firewall configuration
firewall --enabled
# Run the Setup Agent on first boot
firstboot --disable
# System keyboard
keyboard us
# System language
lang en_US
# Use network installation
url --url=$tree
# If any cobbler repo definitions were referenced in the kickstart profile, include them here.
$yum_repo_stanza
# Network information
$SNIPPET('network_config')
# Reboot after installation
reboot

#Root password
rootpw --iscrypted $default_password_crypted
# SELinux configuration
selinux --disabled
# Do not configure the X Window System
skipx
# System timezone
timezone  America/Los_Angeles
# Install OS instead of upgrade
install
# Clear the Master Boot Record
zerombr
# Allow anaconda to partition the system as needed
autopart

%pre
$SNIPPET('log_ks_pre')
$SNIPPET('kickstart_start')
$SNIPPET('pre_install_network_config')
# Enable installation monitoring
$SNIPPET('pre_anamon')
%end

%packages
@core
@base
@server-policy
$SNIPPET('func_install_if_enabled')
yum
openssh-server
openssh-clients
pam
screen
man
wget
vim-enhanced
bind-utils
mlocate
parted
nano
ntpdate
%end

%post --nochroot
$SNIPPET('log_ks_post_nochroot')
%end

%post
$SNIPPET('log_ks_post')
# Start yum configuration
$yum_config_stanza
# End yum configuration
$SNIPPET('post_install_kernel_options')
$SNIPPET('post_install_network_config')
$SNIPPET('func_register_if_enabled')
$SNIPPET('download_config_files')
$SNIPPET('koan_environment')
$SNIPPET('redhat_register')
$SNIPPET('cobbler_register')

echo "192.168.81.144 puppet puppet.audsci.local" >> /etc/hosts

echo "configuring epel repository"
rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

#install puppet agent 
/usr/bin/yum -y install puppet

#create yum puppet install script
cat <<EOF > /etc/init.d/run_puppet_agent
#!/bin/sh
#chkconfig: 2345 90 60
# make sure only runonce after reboot when rpm didn't get install yet
if [[ \`yum list installed | grep puppet\` == "" ]]; then
   # configure Puppet agent installation
   echo "Puppet agent is not installed"
   else   
   # run puppet agent
   puppet agent --test --server=puppet.audsci.local --ca_server=puppet.audsci.local
fi
EOF
chmod a+x /etc/init.d/run_puppet_agent
chkconfig --add run_puppet_agent
chkconfig run_puppet_agent on

# Enable post-install boot notification
$SNIPPET('post_anamon')
# Start final steps
$SNIPPET('kickstart_done')
# End final steps
%end

